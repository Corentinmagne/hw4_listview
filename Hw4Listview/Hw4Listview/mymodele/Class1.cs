﻿using System;
using Xamarin.Forms;

namespace Hw4Listview.Models
{
    public class ModeItem
    {
        public string ModeText
        {
            get;
            set;
        }

        public ImageSource ModeSource
        {
            get;
            set;
        }

        public string ModeDetail
        {
            get;
            set;
        }
        public string urlMode
        {
            get;
            set;
        }
    }
}