﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hw4Listview.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hw4Listview
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class mode : ContentPage
	{
		public mode ()
		{
			InitializeComponent ();

            add_my_list();
        }

        private void add_my_list()
        {
            var imagesForListView = new ObservableCollection<ModeItem>()
            {
                new ModeItem()
                {
                    ModeSource = ImageSource.FromFile("chanel.png"),
                    ModeText = "Chanel",
                    ModeDetail = "Create in 1910",
                    urlMode = "http://www.chanel.com",
                },
                new ModeItem()
                {
                    ModeSource = ImageSource.FromFile("hermes.png"),
                    ModeText = "Hermès",
                    ModeDetail = "Create in 1837.",
                    urlMode = "https://www.hermes.com/us/",
                },
                new ModeItem()
                {
                    ModeSource = ImageSource.FromFile("dior.png"),
                    ModeText = "Christian Dior",
                    ModeDetail = "Create in 1957.",
                    urlMode = "https://www.dior.com/home/en_us",
                },
                new ModeItem()
                {
                    ModeSource = ImageSource.FromFile("louisvuitton.png"),
                    ModeText = "Louis Vuitton",
                    ModeDetail = "Create in 1854.",
                    urlMode = "https://www.louisvuitton.com/",
                },
                new ModeItem()
                {
                    ModeSource = ImageSource.FromFile("prada.png"),
                    ModeText = "Prada",
                    ModeDetail = "Create in 1913.",
                    urlMode = "https://store.prada.com/en/pradaus/",
                },
                new ModeItem()
                {
                    ModeSource = ImageSource.FromFile("gucci.png"),
                    ModeText = "Gucci",
                    ModeDetail = "Create in 1921.",
                    urlMode = "https://www.gucci.com/us/en/",
                },
                new ModeItem()
                {
                    ModeSource = ImageSource.FromFile("balenciaga.png"),
                    ModeText = "Balenciaga",
                    ModeDetail = "Create in 1919.",
                    urlMode = "https://www.balenciaga.com/us",
                },
                new ModeItem()
                {
                    ModeSource = ImageSource.FromFile("guess.png"),
                    ModeText = "Guess",
                    ModeDetail = "Create in 1981.",
                    urlMode = "https://shop.guess.com/en",
                }
            };

            cellmode.ItemsSource = imagesForListView;
        }

        void ListTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            ModeItem itemTapped = (ModeItem)listView.SelectedItem;
            var uri = new Uri(itemTapped.urlMode);
            Device.OpenUri(uri);
        }





    }
}